This is a note of everything I have discovered from SAIL and BSO
As of 2022 ^_^

## LUG Details
- LUG is a Tier Two and it should probably remain a Tier Two.
    - There is no benefit to Moving up that the officers of 2021-2022 could forsee
- LUG's code org index thing is 384403
- LUG only needs ONE service event (Described below)

## SAIL forms/requirements
- As a Tier 2 there are a handlful of requirements. These are all given to you on a list at the fall summit that is held near the start of the Fall Semester.
- The List for LUG in the 2021-2022 year is as follows
    - Goals Form (Fall Semester)
        - A form that asks questions about the club and what the club does
    - BSO Meeting (Fall and Spring Semesters)
        - Meet with ANYONE from BSO. You just join the call/office hours and just give the club name and they ask if you have any questions
        - There is a Fall Semester Meeting and a Spring Semester Meeting
    - Goals Evaluations (Spring Semester)
        - Answer if you achieved the goals written in the Fall
        - Very vague answers such as "yes we did because..."
    - Officer Transitions (Spring Semester)
        - Form asking how officer transitions will occur and when
        - You have to write something down but so long as the new officers take control before the next semester everything is good regardless of what was written down.
    - Service Events (Spring Semester)
        - The service event is something open to the campus.
        - It is very vague and I never got an answer for it after asking several times
        - Historically we use the Install Fest day and the NEW! Self Host day. These two events work just fine and fill the requirement
    - Summary Allocations
        - A form that describes what you "think" you will use next semester.
        - It does not have to be specific it is just your guess at the time it is due
        - I personally recommend no budget as dealing with all the rules is a pain.
        - You say what you are planning on doing between that form and the end of the semester to not cause any roll over to occur
        - Roll over is previous semester money and BSO wants that to be as close to zero as possible (We left like 500-600 in there whoops!) 
    - FINAL Allocations
        - This is similar to the form above but it is what you are officially requesting from BSO
        - You detail events and purchases that are planned for next semester.
        - You plan every purchase and then you get an email that is a big spreadsheet and slideshow showing the budget of EVERY club including LUG!
        - You either get everything you requested or you get less than what you requested because BSO had to limit money somewhere and for some reason.

## Room reservation
- To reserve a room i recommend a week before school as no other club usually reserves at this time (I hope) and the registrar takes for EVER to actually confirm the room
- Go to https://events.mines.edu/EMSWebApp/ and follow the instructions.
- I only used it once as it is very new at the time of writing.
- If LUG does not appear in the list of clubs or departments or whatever it is called in the website then contact the email that appears in one of the question mark options. (reservations@mines.edu)

- I believe my information is still there for the club (that or the previous president) so be sure to email reservations@mines.edu regardless to change that so SAIL/registrar has the correct person on file


## Good Information
- What you request and gets allocated is LUGs money. In theory it should never go away so that 500-600 or however much officers of 2021-22 left should still be there if it has not been spent by previous presidents between you the reader and the year 2022!

- Treasurer training is good but also useless. They go over rules rather than how to spend money. I am still not sure how to actually spend money... Maybe go ask the ACM treasurer i'm sure they will know(I hope..)

## Spending Money
- Spending money is the biggest challenge of running a club

- You can't spend more than 25% of the budget on food
    - (We did it once and we got in a lot of hot water in 2019)

- Shirts, bottles, apparel in general can't be bought with BSO money. 
    - There is some loop or way to do it with approved sellers but if it has the school logo you HAVE to get it approved by someone in SAIL (I forgot who and it probably is going to change between when i write this and when you need it!)
    - The LUG stickers have been previously bought by a LUG president who bit the bullet and bought a massive amount of them

- Everything bought is Government property by law or something. Nothing can be given away and anything more expensive than 500$ or some random number is kept track of on a list somewhere in SAIL and it must be confirmed that it was transfered from the previous president to the current president to SAIL.

## 
