## 2019-08-23
* Celebration of Mines

## 2019-08-29
* LUG Intro Presentation

## 2019-09-05
* Containerization (Liam Warfield)

## 2019-09-12
* Distros (Jordan - centOS; David - Gentoo; Jesus - WSL; Robby - Arch)

## 2019-09-19
* Linux files (Nick)

## 2019-09-26
* Gaming on Linux (Jesus)

## 2019-09-27
* LAN Party

## 2019-10-03
* Text Editors (Jo - ed; Jordan - vim; Joseph - kakoune; David - emacs)

## 2019-10-10
* Graphics on Linux (Jesus, Jo, Sam Sartor)

## 2019-10-17
* Xest (Jack)

## 2019-10-24
* A Byte To Eat - Instead of Meeting

## 2019-10-31
* LISP-machines (Jo)

## 2019-11-07
* Open Protocols (Sumner, Robby)

## 2019-11-14
* Kubernetes (Liam Warfield)

## 2019-11-21
* Dank Memes

## 2019-11-22
* LAN Party

## 2019-11-28
* Thanksgiving Break - No Meeting

## 2019-12-05
* Dead Week

## 2019-12-12
* Finals Week - No Meeting
