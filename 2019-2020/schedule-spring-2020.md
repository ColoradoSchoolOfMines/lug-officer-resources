## 2020-01-09
* Interesting Programming Languages (Jo: FORTRAN, Fisher: Rust, Ben: Swift, Robby: Elixir)

## 2020-01-16
* C-MAPP Party - No LUG

## 2020-01-23
* Coq (Joseph)

## 2020-01-30
* Monads (Miles)

## 2020-02-06
* Turing-Completeness (Jo, Josiah, Jared)

## 2020-02-13
* tmux (Jordan) 

## 2020-02-20
* Interesting Database Software (Liam Warfield, Sam Warfield, Fisher, Joseph)

## 2020-02-27
* CI/CD (Sam Warfield)

## 2020-03-05
* Non-UNIX Open-Source Operating Systems

## 2020-03-12
* BitTorrent (David)

## 2020-03-19
* COVID-19 Class Cancellations - No LUG

## 2020-03-26
* Spring Break - No LUG 

## 2020-04-02
* 

## 2020-04-09
* 

## 2020-04-16
* 

## 2020-04-23
* Elections + Movie Night (?)

## 2020-04-30
* Dead Day - Show and Tell (?)

## 2020-05-07
* Finals Week - No LUG

