# Officer Transition Checklist

## General
- [ ] Email LUG mailing list with election results
- [ ] Hold meeting where old officers brief new officers on their position's responsibilities
- [ ] Hand over CS Department cabinet keys to new Treasurer and Secretary
- [ ] Train new Secretary on room reservation
- [ ] Train new President on voting software usage
- [ ] Train new officers on Mozzarella usage

## Web Infrastructure
- [ ] Update contact page on lug.mines.edu
- [ ] Update officer information in Engage
- [ ] Update officer user permissions in Mozzarella (lug.mines.edu)
- [ ] Hand over mailing list password to new President

## Git
- [ ] Give new officers appropriate access to GitLab repositories
- [ ] Remove old officers' access to GitLab repositories, as appropriate

## Matrix
- [ ] Add new officers to LUG officers Matrix chat + set user power level, as needed
- [ ] Remove members who are no longer officers from LUG officers Matrix chat
- [ ] Downgrade user power levels of members who are no longer officers in LUG Matrix chat

## Arrakis
- [ ] Hand over Arrakis password(s) and location information to new officers
- [ ] Update contact information on paper attached to Arrakis

