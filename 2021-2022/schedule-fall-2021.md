## 27-08-2021
* Celebration of Mines

## 02-09-2021
* LUG Intro & Install Festival

## 09-09-2021
* Overview of Linux Terminology

## 16-09-2021
* TBD

## 23-09-2021
* TBD

## 30-09-2021
* TBD

## 07-10-2021
* TBD

## 14-10-2021
* TBD

## 21-10-2021
* TBD

## 28-10-2021
* TBD

## 04-11-2021
* TBD

## 11-11-2021
* TBD

## 18-11-2021
* TBD

## 25-11-2021
* Thanksgiving Break - No LUG

## 02-12-2021
* TBD

## 09-12-2021
* Dead Week
