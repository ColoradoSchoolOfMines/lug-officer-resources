(Crossed-out items have been scheduled.)

## Fall 2019
* ~~Penguinitis Redux~~
* ~~Containerization (Liam Warfield)~~
* ~~Distros (Jordan, David, Jesus)~~
* ~~Linux File System (Nick Carnival)~~
* ~~Gaming on Linux (Jesus)~~
* ~~text editors (Jordan, David, Joseph, Jo)~~
* ~~Graphics on Linux (Sam Sartor, Jo, Jesus)~~
* ~~Xest WM (Jack)~~
* ~~LISP (Jo)~~
* ~~Subsonic + Alternate Clients (Sumner, Robby)~~
* ~~Kubernetes (Liam Warfield)~~
* ~~Dank Memes~~


## Spring 2020
* ~~Interesting Programming Languages: FORTRAN, Swift, Elixir, Rust (Jo, Ben Carlson, Robby, Fisher)~~
* ~~A Taste of Coq (Joseph)~~
* ~~Monads (Miles)~~
* ~~Esoteric Programming Languages: Game of Life, C++ templating, PowerPoint (Jo, Josiah, Jared)~~
* ~~Interesting Database Software (Liam Warfield, Sam Warfield, Fisher, Joseph)~~
* ~~tmux (Jordan)~~
* ~~CI/CD (Sam Warfield)~~
* ~~non-UNIX free operating systems: FreeDOS, ReactOS, TempleOS, CollapseOS, Redox (Jo, Jack, Jesus, Sam Warfield, Fisher)~~
* ~~BitTorrent (David)~~
* BSD (Jordan)
* Scaling software & cloud-native apps (Liam Warfield)
* How to make a driver (Robby)
* wasm (Sam Warfield, Sam Sartor)
* Open-Source Scientific Computing (Joseph)
* JIT compiling (Sam Sartor)
* The C Preprocessor (Jack Rosenthal)
* LLVM (Fisher)


## Fall 2020
* ~~Making Linux Look Cool (Joseph)~~
* ~~Coreboot/Libreboot (Miles)~~
* ~~NixOS (Jack Garner)~~
* ~~On Keyboards and Things v2.0 (Jack Rosenthal)~~
* ~~IPC (Jordan)~~
* ~~git tools lightning talks (Jack Rosenthal - Gerrit, SourceHut, GitLab, GitHub, Bitbucket, Fisher - pijul, Liam Warfield - perforce)~~
* The Quantified Self (Joseph)
* Gerrit (Jack Rosenthal)
* ~~Text Editors (Fraser - EMACS, Cyrus - JED, Jack Garner - neovim, Jack Rosenthal - teco)~~
* ~~Chromebook hacking (Jack Rosenthal)~~
* ~~UI Toolkits (Sumner, Dorian)~~


## Spring 2021
* ~~The JVM (Jo, Miles)~~
* ~~Kconfig (Jack Rosenthal)~~
* Gentoo (Jack Rosenthal, ???)
* ~~WINE (Jesus)~~
* regex (Miles)


## Special Activities
* Self-Hosting Workshop
* Show and Tell
