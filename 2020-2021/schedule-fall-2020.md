## 28-08-2020
* Celebration of Mines

## 03-09-2020
* LUG Intro

## 10-09-2020
* Making Linux Look Cool

## 17-09-2020
* Git Hosting & Code Review Tools

## 24-09-2020
* NixOS

## 01-10-2020
* Text Editors

## 08-10-2020
* Chromebook Hacking

## 15-10-2020
* Website Setup Walkthrough

## 22-10-2020
* Backups

## 29-10-2020
* Coreboot/Libreboot

## 05-11-2020
* IPC

## 12-11-2020
* On Keyboards and Things (aka Something) v2.0

## 19-11-2020
* UI Toolkits

## 26-11-2020
* Thanksgiving Break - No LUG

## 03-12-2020
* Dank Memes

## 10-12-2020
* Dead Week
