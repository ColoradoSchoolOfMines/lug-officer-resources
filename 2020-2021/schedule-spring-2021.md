## 14-01-2021
* No LUG

## 21-01-2021
* C-MAPP meeting - No LUG

## 28-01-2021
* WINE

## 04-02-2021
* Forth

## 11-02-2021
* KConfig

## 18-02-2021
* GRUB

## 25-02-2021
* No LUG

## 04-03-2021
* Filesystems

## 11-03-2021
* UNIX utils

## 18-03-2021

## 25-03-2021

## 01-04-2021
* Spring Break - No LUG

## 08-04-2021
* The JVM

## 15-04-2021

## 22-04-2021

## 29-04-2021
* 2021-22 Officer Elections

## 03-05-2021
* Dead Week

